import { Routes } from '@angular/router';
import { HomeComponent } from './home/index';
import { AboutComponent } from './about';
import { NoContentComponent } from './no-content';

export const ROUTES: Routes = [
  { path: '',      component: HomeComponent },
  { path: 'home',  component: HomeComponent },
  { path: 'c', loadChildren: './category/category.module#CategoryModule' },
  { path: '**',    component: NoContentComponent },
];
