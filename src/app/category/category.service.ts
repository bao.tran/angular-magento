import { Injectable } from '@angular/core';

import { DataService } from '../shared/service/data.service';
import { OauthRequestService } from '../shared/service/oauth-request.service';

@Injectable()
export class CategoryService extends DataService {
  public constructor(
    public http: OauthRequestService,
  ) {
    super(http, '/V1/categories');
  }
}
