import { NgModule } from '@angular/core';

import { CategoryService } from './category.service';
import { CategoryComponent } from './category.component';
import { CategoryRoutes } from './category.routes';
import { OauthRequestService } from '../shared/service/oauth-request.service';

/**
 * `AppModule` is the main entry point into Angular2's bootstraping process
 */
@NgModule({
  declarations: [
    CategoryComponent
  ],
  /**
   * Import Angular's modules.
   */
  imports: [
    CategoryRoutes
  ],
  /**
   * Expose our Services and Providers into Angular's dependency injection.
   */
  providers: [
    CategoryService,
    OauthRequestService
  ]
})
export class CategoryModule {}
