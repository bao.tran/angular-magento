import {
  Component,
  OnInit
} from '@angular/core';

import { AppState } from '../app.service';
import { CategoryService } from './category.service';

@Component({
  /**
   * The selector is what angular internally uses
   * for `document.querySelectorAll(selector)` in our index.html
   * where, in this case, selector is the string 'home'.
   */
  selector: 'category',  // <category></category>
  /**
   * Every Angular template is first compiled by the browser before Angular runs it's compiler.
   */
  templateUrl: './category.component.html'
})
export class CategoryComponent implements OnInit {
  /**
   * Set our default values
   */
  public localState = { category: {} };
  /**
   * TypeScript public modifiers
   */
  constructor(
    public appState: AppState,
    public categoryService: CategoryService
  ) {}

  public ngOnInit() {
  }

  public submitState(value: string) {
    console.log('submitState', value);
    this.appState.set('category', value);
    this.localState.category = {};
  }
}
