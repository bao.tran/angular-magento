import { Injectable } from '@angular/core';

import deepmerge from 'deepmerge';
import * as queryString from 'qs';

import { OauthRequestService } from './oauth-request.service';

export class DataService {
  protected searchUrl = '';
  protected defaultPagination = {
    searchCriteria: {
      sortOrders: [{field: 'id', direction: 'asc'}],
      pageSize: 10,
      currentPage: 0,
    }
  };

  constructor(
    public http: OauthRequestService,
    public apiUrl: string
  ) {
  }

  public getAll() {
    return this.search({}, {});
  }

  public getById(id) {
    return this.http.get(this.apiUrl + id + '?storeId=1')
    .map((res) => res.json());
  }

  public search(data, options) {
    let pagination = JSON.parse(JSON.stringify(this.defaultPagination));

    // merge options passed with default Pagination
    deepmerge(pagination, options);

    // prepare params search for magento2
    let keys = Object.keys(data);

    let filterGroups = null;

    for (let key of keys) {
      if (data[key] !== '') {
        filterGroups = filterGroups || [{ filters: [] }];

        const filter = {
          field: key,
          value: '%25' + data[key] + '%25',
          conditionType: 'like',
        };
        if (key === 'category_id') {
          filter.value = data[key];
          filter.conditionType = 'eq';
        }
        filterGroups.push({
          filters: [filter]
        });
      }
    }

    // parse to query string to submit
    let tmpObj = { searchCriteria: { filterGroups }};

    // another haven't index
    let qs = queryString.stringify(pagination, {encode: false, arrayFormat: 'indices'});

    // filterGroups have index
    if (filterGroups) {
      qs += '&' + queryString.stringify(tmpObj, {encode: false});
    }

    let url = this.apiUrl + 'search?' + qs;
    if (this.searchUrl) {
      url = this.searchUrl + '?' + qs;
    }

    return this.http.get(url)
      .map((res) => res.json());
  }
}
