import { Injectable } from '@angular/core';
import { Http,
  Request,
  Response,
  RequestOptions,
  RequestOptionsArgs,
  XHRBackend,
  Headers,
  RequestMethod
} from '@angular/http';

import * as OAuth from 'oauth-1.0a';
import CryptoJS from 'crypto-js';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

interface AccessToken {
  key: string;
  secret: string;
}

interface Consumer {
  key: string;
  secret: string;
}

@Injectable()
export class OauthRequestService extends Http {
  public oauth;
  public consumer: Consumer;
  public accessToken: AccessToken;

  constructor(
    public backend: XHRBackend,
    public defaultOptions: RequestOptions
  ) {
    super(backend, defaultOptions);
    this.consumer = {
      key: 'enapx2g0d9n25cpgy9o12tmn5y4wwntp',
      secret: 'nmr1i1gy9o8jmwk438mmq1okvcx068k6'
    };
    this.accessToken = {
      key: '9womov57iq8o56pgiwee3jbi6df8x80w',
      secret: '7xvl4pxs2eroy27dke3uxan8p3w61i87'
    };
    this.oauth = OAuth({
      consumer: this.consumer,
      signature_method: 'HMAC-SHA1',
      hash_function: this.hashSha1
    });
  }

  public hashSha1(baseString, key) {
    return CryptoJS.HmacSHA1(baseString, key).toString(CryptoJS.enc.Base64);
  }

  public request(url: string | Request, options?: RequestOptionsArgs): Observable<Response> {
    if (url instanceof Request) {
      this.getRequestOptionArgs(url.url, url);
      return super.request(url, options);
    } else {
      return super.request(url, this.getRequestOptionArgs(url, options));
    }
  }

  protected getRequestOptionArgs(url: string, options?: RequestOptionsArgs | Request, body?: any): RequestOptionsArgs {
    if (options == null) {
      options = new RequestOptions();
    }
    if (options.headers == null) {
      options.headers = new Headers();
    }
    options.headers.append('Content-Type', 'application/json');
    const header = this.oauth.toHeader(this.oauth.authorize({
      url,
      method: RequestMethod[options.method].toUpperCase(),
      data: body,
    }, this.accessToken
    ));
    options.headers.append('Authorization', header.Authorization);
    return options;
  }
}
